@Override
public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.main);
}
 
@Override
protected void onStart() {
    Resources res = getResources();
    /* 更改语系为JAPAN */
    Configuration conf = res.getConfiguration();
    conf.locale = Locale.JAPAN;
    DisplayMetrics dm = res.getDisplayMetrics();
    /* 保存语系变更 */
    res.updateConfiguration(conf, dm);
 
    super.onStart();
}
 
/*
 * <?xml version="1.0" encoding="utf-8"?> <resources> <string
 * name="app_name">EX03_23_1</string> <string
 * name="str1">すべての道はローマにつながる.</string> <string
 * name="str2">ローマは一日に作成されませんでした.</string> <string
 * name="str3">知識は力です.</string> </resources>
 */
